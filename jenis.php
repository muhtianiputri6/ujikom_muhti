<?php
include "head.php";
include "koneksi.php";
?>

<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data Jenis</h3>                                    
        </div>
        <a href="tambah_jenis.php"><button class="btn btn-github" style="margin-left: 10px;">Tambah</button></a>
        <div class="box-body table-responsive">
            
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Jenis</th>
                        <th>Kode Jenis</th>
                        <th>Keterangan</th>
                        <th>Option</th>
                    </tr>
                </thead>
                <?php
                include "koneksi.php";
                $no=1;
                $pilih=mysqli_query($konek, "SELECT * FROM jenis ORDER BY id_jenis ASC");
                while($data=mysqli_fetch_array($pilih)){
                ?>
                <tbody>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?=$data['nama_jenis']; ?></td>
                        <td><?=$data['kode_jenis']; ?></td>
                        <td><?=$data['keterangan'];?></td>
                        <td>
                            <a class="btn btn-github" href="edit_jenis.php?id_jenis=<?php
                                echo $data['id_jenis'];?>"><i class="fa fa-edit"></i></a>
                            <a onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data Ini?')"
                                class="btn btn-danger" href="hapus_jenis.php?id_jenis=<?php echo $data['id_jenis'];?>"><i class="fa fa-trash-o"></i></a> 
                        </td>
                    </tr>
                </tbody>
                <?php 
            	$no++;    
                } 
                ?>
            </table>
        </div>
    </div>
</section>

<?php include "footer.php"; ?>