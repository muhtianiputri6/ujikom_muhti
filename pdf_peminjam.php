<?php
include 'koneksi.php';
include 'pdf/fpdf.php';

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(5,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);

$pdf->SetX(13);
$pdf->MultiCell(19.05,0.5,'SMKN 1 CIOMAS',0,'L');
$pdf->SetX(12.05);
$pdf->MultiCell(19.05,0.5,'Telpon : 0812XXXXXXXX',0,'L');
$pdf->SetFont('Arial','B',10);
$pdf->SetX(10);
$pdf->MultiCell(19.05,0.5,'Jl. Laladon Desa Laladon RT 02/01 Kec. Ciomas Bogor',0,'L');
$pdf->SetX(12);
$pdf->MultiCell(19.05,0.5,'Website : wwww.smkn1ciomas.com',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);
$pdf->Line(1,3.2,28.5,3.2);
$pdf->SetLineWidth(0);
$pdf->Line(1,3.1,28.5,3.1);
$pdf->ln(1);


$pdf->SetFont('Arial','B',14);
$pdf->Cell(20,0.7,"Laporan Peminjaman",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Nama Barang', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Jumlah Pinjam', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Tanggal Pinjam', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Status Peminjaman', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Nama Pegawai', 1, 1, 'C');

$pdf->SetFont('Arial','',10);
$no=1;
$query=mysqli_query($konek, "SELECT * FROM peminjaman left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai JOIN detail_pinjam ON detail_pinjam.id_peminjaman=peminjaman.id_peminjaman LEFT JOIN inventaris on peminjaman.id_inventaris=inventaris.id_inventaris WHERE peminjaman.status_peminjaman='Dipinjam'");
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(4, 0.8, $lihat['nama'],1, 0, 'C');
	$pdf->Cell(4, 0.8, $lihat['jumlah_pinjam'],1, 0, 'C');
	$pdf->Cell(4, 0.8, $lihat['tanggal_pinjam'], 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['status_peminjaman'], 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['nama_pegawai'],1, 1, 'C');
	

	$no++;
}

$pdf->Output("laporan_peminjaman.pdf","I");

?>


