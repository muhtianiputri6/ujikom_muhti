<?php
include "head.php";
include "koneksi.php";
?>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Pengembalian</h3>                                    
				</div>

					<div class="box-body table-responsive">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama Barang</th>
								<th>Tanggal Kembali</th>
								<th>Status</th>
								<th>Peminjam</th>
								<th>Aksi</th>
							</tr>
						</thead>

						<?php
						include "koneksi.php";
						$no=1;
						$pilih=mysqli_query($konek, "SELECT * FROM peminjaman left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai JOIN detail_pinjam ON detail_pinjam.id_peminjaman=peminjaman.id_peminjaman LEFT JOIN inventaris on peminjaman.id_inventaris=inventaris.id_inventaris where status_peminjaman='Dikembalikan' order by tanggal_kembali desc");	
						while($data=mysqli_fetch_array($pilih)){
							?>
							<tbody>
								<tr>
									<td><?=$no++;?></td>
									<td><?=$data['nama'];?></td>
									<td><?=$data['tanggal_kembali'];?></td>
									<td><?=$data['status_peminjaman'];?></td>
									<td><?=$data['nama_pegawai'];?></td>
									<td>
										<button type="button" class="btn btn-github btn-md" data-toggle="modal" data-target="#myModal-<?=$data['id_peminjaman'];?>">Detail</button>

                                    <div id="myModal-<?=$data['id_peminjaman'];?>" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- konten modal-->
                                            <div class="modal-content">
                                                <!-- heading modal -->
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Detail Pinjam</h4>
                                                </div>
                                                <!-- body modal -->
                                                <div class="modal-body">
                                                	<table>
                                                    <tr>
                                          				<th>Nama Barang</th>
                                                    	<td>:</td>
                                                    	<td><?=$data['nama'];?></td>
                                                    </tr>
                                                    <tr>
                                                    	<th>Jumlah Pinjam</th>
                                                    	<td>:</td>
                                                    	<td><?=$data['jumlah_pinjam'];?></td>
                                                    </tr>
                                                    <tr>
                                                    	<th>Tanggal Pinjam</th>
                                                    	<td>:</td>
                                                    	<td><?=$data['tanggal_pinjam'];?></td>
                                                    </tr>
                                                    <tr>
                                                    	<th>Tanggal Kembali</th>
                                                    	<td>:</td> 
                                                    	<td><?=$data['tanggal_kembali'];?></td>
                                                    </tr>
                                                    <tr>
                                                    	<th>Nama Peminjam</th>
                                                    	<td>:</td>
                                                    	<td><?=$data['nama_pegawai'];?></td>
                                                	</tr>
                                                
                                                </table>
                                                </div>
                                                <!-- footer modal -->
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
									</td>


								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include "footer.php"; ?>