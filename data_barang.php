<?php
session_start();
if($_SESSION['level']==""){
	header("location:login.php");
}
?>

<?php include"head.php"; ?>
<div class="content">
	<h4 class="page-header">
		Kategori Perjurusan
	</h4>
	<!-- Solid boxes -->
	<a href="tambah_barang.php"><button class="btn btn-github" style="margin-left: 10px;">Tambah</button></a>
	<br>
	<br>
	<div class="row">
		<div class="col-md-4">
			<!-- Default box -->
			<div class="box box-solid box-primary">
				<div class="box-header">
					<h3 class="box-title">Rekayasa perangkat Lunak</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-default btn-sm" data-widget="collapse"></button>
					</div>
				</div>
				<div class="box-body">
					<center><img src='img/rpl1.jpg' width="75%" height="75%"/></center><br>
					<center><a href="admin.php?jurusan=RPL"><button class="btn btn-github">Lihat</button></a></center>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->

		<div class="col-md-4">
			<!-- Primary box -->
			<div class="box box-solid box-primary">
				<div class="box-header">
					<h3 class="box-title">Teknik Kendaraan Ringan</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-default btn-sm" data-widget="collapse"></button>
					</div>
				</div>
				<div class="box-body">
					<center><img src='img/tkr1.jpg' width="50%" height="50%"/></center><br>
					<center><a href="admin.php?jurusan=TKR"><button class="btn btn-github">Lihat</button></a></center>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->

		<div class="col-md-4">
			<!-- Info box -->
			<div class="box box-solid box-primary">
				<div class="box-header">
					<h3 class="box-title">Broadcasting</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-default btn-sm" data-widget="collapse"></i></button>
					</div>
				</div>
				<div class="box-body">
					<center><img src='img/bc.png' width="68%" height="68%"/></center><br>
					<center><a href="admin.php?jurusan=Broadcast"><button class="btn btn-github">Lihat</button></a></center>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->

	<div class="row">
		<div class="col-md-4">
			<!-- Danger box -->
			<div class="box box-solid box-primary">
				<div class="box-header">
					<h3 class="box-title">Teknik Pengelasan</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-default btn-sm" data-widget="collapse"></button>
					</div>
				</div>
				<div class="box-body">
					<center><img src='img/tpl1.png' width="75%" height="75%"/></center><br>
					<center><a href="admin.php?jurusan=TPL"><button class="btn btn-github">Lihat</button></a></center>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->

		<div class="col-md-4">
			<!-- Success box -->
			<div class="box box-solid box-primary">
				<div class="box-header">
					<h3 class="box-title">Animasi</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-default btn-sm" data-widget="collapse"></button>
					</div>
				</div>
				<div class="box-body">
					<center><img src='img/animasi1.png' width="50%" height="50%"/></center><br>
					<center><a href="admin.php?jurusan=Animasi"><button class="btn btn-github">Lihat</button></a></center>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->

		<div class="col-md-4">
			<!-- Warning box -->
			<div class="box box-solid box-primary">
				<div class="box-header">
					<h3 class="box-title">Barang Lainnya</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-default btn-sm" data-widget="collapse"></button>
					</div>
				</div>
				<div class="box-body">
					<center><img src='img/deliver.png' width="50%" height="50%"/></center><br>
					<center><a href="admin.php?jurusan=Lainnya"><button class="btn btn-github">Lihat</button></a></center>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div>

<?php include "footer.php"; ?>