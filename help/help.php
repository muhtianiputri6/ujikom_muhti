<!DOCTYPE html>
<html>
<head>
	<title>Help</title>
</head>
<body>
<div class="col-md-8">
			<!-- Default box -->
			<div class="box box-solid box-primary">
				<div class="box-header">
					<h3 class="box-title">Manual Program</h3>
				</div>
				<div class="box-body">
					<table width="50%" border="1">
						<tr>
							<td>1.</td>
							<td>Login sebagai admin : username=admin , password=admin. Login sebagai operator : username=operator , password=operator . Login sebagai user yaitu username : muhtianiputri, password : muhtiani123</td>
						</tr>
						<tr>
							<td>2.</td>
							<td>Pada halaman admin, data inventaris dapat dilihat sesuai jurusan. Dan barang diluar jurusan atau bersifat umum seperti proyektor terdapat pada bagian barang lainnya. Pada tabel inventaris terdapat jenis yang berisi nama-nama jurusan yang sudah ada. </td>
						</tr>
						<tr>
							<td>3.</td>
							<td>Peminjaman barang pada halaman admin dan operator, barang dapat dipinjam sesuai jurusan. Jika ingin meminjam laptop maka bisa membuka pada kelompok jurusan RPL yang kemudian terdapat form untuk di isi. Dan data yang sudah dipinjam akan masuk ke dalam fitur data pinjam yang berisi tabel data peminjaman. Lalu jumlah barang inventaris akan berkurang setelah dipinjam.</td>
						</tr>
						<tr>
							<td>4.</td>
							<td>Jika barang ingin dikembalikan, maka dapat dilihat pada tabel data pinjam dan bisa langsung klik button kembalikan, lalu barang tersebut akan masuk ke dalam tabel pengembalian dan jumlah barang inventaris akan menambah sesuai jumlah yang sudah dikembalikan tadi. </td>
						</tr>
						<tr>
							<td>5.</td>
							<td>Pada fitur laporan terdapat data yang bisa dicetak melalui cetak excel dan cetak pdf yaitu ada data peminjaman, pengembalian, dan data inventaris.</td>
						</tr>
						<tr>
							<td>6.</td>
							<td>Pada backup database, database akan langsung terdownload apabila diklik.</td>
						</tr>
						<tr>
							<td>7.</td>
							<td>Dan dalam fitur kelola user, dapat ditambahkan apabila terdapat petugas baru.</td>
						</tr>
						<tr>
							<td>8.</td>
							<td>Di dalam aplikasi inventaris ini, fitur pengembalian dan forgot password masih belum berjalan dengan baik. Dan belum terdapat halaman user.</td>
						</tr>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
</body>
</html>