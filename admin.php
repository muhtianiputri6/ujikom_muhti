<?php
include "head.php";
?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Inventarisir</h3>                                    
                </div>
                
                <div class="box-body table-responsive">

                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Barang</th>
                                <th>Kondisi</th>
                                <th>Keterangan</th>
                                <th>Jumlah</th>
                                <th>Jenis</th>
                                <th>Tanggal Register</th>
                                <th>Ruang</th>
                                <th>Kode Barang</th>
                                <th>Petugas</th>
                                <th>Option</th>
                            </tr>
                        </thead>

                        <?php
                        include "koneksi.php";
                        if(isset($_GET['jurusan'])){

                            $muhti = $_GET['jurusan'];
                            $pilih=mysqli_query($konek, "SELECT * FROM inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis WHERE nama_jenis='$muhti'");
                        }
                        else{
                            $pilih=mysqli_query($konek, "SELECT * from inventaris");
                        }
                        $no=1;

                        while($data=mysqli_fetch_array($pilih)){
                            ?>
                            <tbody>
                                <tr>
                                    <td><?=$no++;?></td>
                                    <td><?=$data['nama']; ?></td>
                                    <td><?=$data['kondisi']; ?></td>
                                    <td><?=$data['keterangan'];?></td>
                                    <td><?=$data['jumlah'];?></td>
                                    <td><?=$data['id_jenis'];?></td>
                                    <td><?=$data['tanggal_register'];?></td>
                                    <td><?=$data['id_ruang'];?></td>
                                    <td><?=$data['kode_inventaris'];?></td>
                                    <td><?=$data['id_petugas'];?></td>
                                    <td>
                                        <a class="btn btn-github" href="edit_barang.php?id_inventaris=<?php
                                        echo $data['id_inventaris'];?>"><i class="fa fa-edit"></i></a>
                                        <a onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data Ini?')"
                                        class="btn btn-danger btn-flat" href="hapus_barang.php?id_inventaris=<?php echo $data['id_inventaris'];?>"><i class="fa fa-trash-o"></i></a> 
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "footer.php"; ?>