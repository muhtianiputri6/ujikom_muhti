<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Inventaris</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

    
    <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
    html, body {
    overflow-x: hidden!important;
    font-family: 'Source Sans Pro', sans-serif;
    -webkit-font-smoothing: antialiased;
    min-height: 100%;
    background: #f9f9f9;
}
    .skin-black .navbar {
        background-color: #827d7d;
        border-bottom: 0px solid #eee;
    }
.skin-black .logo {
    background-color: #827d7d;
    color: #f9f9f9;
}
.skin-black .left-side {
    background: #cbd0d6;
}
.skin-black .sidebar a {
    color: #080808;
}
.skin-black .sidebar > .sidebar-menu > li:first-of-type > a {
    border-top: 0px solid #827d7d;
}
.skin-black .logo {
    background-color: #827d7d;
    color: #f9f9f9;
}
.skin-black .user-panel > .info, .skin-black .user-panel > .info > a {
    color: #080707;
}
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 14px;
    line-height: 1.428571429;
    color: #080707;
    background-color: #fff;
}
.user-panel > .image > img {
    width: 100px;
    height: 100px;
}
.table-striped > tbody > tr:nth-child(odd) > td, .table-striped > tbody > tr:nth-child(odd) > th {
    background-color: #cbd0d6;
}

.pagination>.active>a, .pagination>.active>span, .pagination>.active>a:hover, .pagination>.active>span:hover, .pagination>.active>a:focus, .pagination>.active>span:focus {
    z-index: 2;
    color: #fff;
    cursor: default;
    background-color: #444444;
    border-color: #444444;
}
.page-header {
    padding-bottom: 9px;
    margin: 10px 0 20px;
    border-bottom: 2px solid #827d7d;
}
.box.box-solid.box-primary > .box-header {
    color: #fff;
    background: #827d7d;
    background-color: #827d7d;
}
</style>
</head>
<body class="skin-black">

    <header class="header">
        <a class="logo">
           <h4 style="margin-top: 15px; margin-bottom: 20px;">SCHOOL INVENTORY</h4>
       </a>

       <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">

    <aside class="left-side sidebar-offcanvas">                
        <section class="sidebar">

            <div class="user-panel">
                
                <center><img src="img/admin.png" class="img-circle" alt="User Image" style="margin-top: 10px; margin-bottom: 20px" height="70%" width="70%" />
                </center>
                
            </div>

            <ul class="sidebar-menu">

                <li class="treeview">
                    <a href="peminjaman.php">
                        <i class="fa fa-pencil"></i> <span>Peminjaman</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="peminjaman.php"><i class="fa fa-angle-double-right"></i> Menu Pinjam</a></li>
                        <li><a href="data_peminjaman.php"><i class="fa fa-angle-double-right"></i> Data Pinjam</a></li>                               
                    </ul>
                </li>

            
                <li class="">
                    <a href="../login.php">
                        <i class="fa fa-sign-out"></i> <span>Logout</span>
                    </a>
                </li>

            </ul>
        </section>
    </aside>

    <aside class="right-side">        
        <!-- Content Header (Page header) -->
        <section class="content-header">


        </section>
        <!-- </aside> -->
