DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("3","65","1","15");
INSERT INTO detail_pinjam VALUES("4","65","1","16");
INSERT INTO detail_pinjam VALUES("5","65","1","17");
INSERT INTO detail_pinjam VALUES("6","65","1","18");
INSERT INTO detail_pinjam VALUES("7","65","3","19");
INSERT INTO detail_pinjam VALUES("8","65","1","20");
INSERT INTO detail_pinjam VALUES("9","73","1","21");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `kondisi` varchar(50) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` varchar(50) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `id_ruang` varchar(50) NOT NULL,
  `kode_inventaris` varchar(50) NOT NULL,
  `id_petugas` varchar(50) NOT NULL,
  PRIMARY KEY (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("65","Headphone","Baik","Barang RPL","50","3","2019-03-29 14:54:14","Lab RPL","RPL-001","Admin");
INSERT INTO inventaris VALUES("66","Infocus","Baik","Barang Lainnya","12","8","2019-03-31 14:42:07","Lainnya","DLL-001","Admin");
INSERT INTO inventaris VALUES("67","Laptop Acer","Baik","Barang RPL","40","3","2019-03-31 15:51:10","Lab RPL","RPL-002","Admin");
INSERT INTO inventaris VALUES("68","Kamera","Baik","Barang Broadcast","3","7","2019-04-01 02:53:38","Ruang Broadcast","BC-001","Admin");
INSERT INTO inventaris VALUES("69","MIC","Baik","Barang Broadcast","3","7","2019-04-01 03:03:07","Ruang Broadcast","BC-002","Admin");
INSERT INTO inventaris VALUES("70","PC","Baik","Barang Animasi","2","5","2019-04-05 10:23:52","Lab Animasi","BVB21H","Admin");
INSERT INTO inventaris VALUES("71","Las","Baik","Barang TPL","5","6","2019-04-06 14:59:29","Ruang Las","BHG7679","Admin");
INSERT INTO inventaris VALUES("72","Obeng","Baik","Barang TKR","10","4","2019-04-06 15:23:53","Bengkel TKR","NB21B2V","Admin");
INSERT INTO inventaris VALUES("73","Laptop-Lenovo","Baik","Barang RPL","39","3","2019-04-06 15:56:12","Lab RPL","C21V2GB2","Admin");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(100) NOT NULL,
  `kode_jenis` varchar(20) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("3","RPL","3","Barang RPL");
INSERT INTO jenis VALUES("4","TKR","4","Barang TKR");
INSERT INTO jenis VALUES("5","Animasi","5","Barang Animasi");
INSERT INTO jenis VALUES("6","TPL","6","Barang TPL");
INSERT INTO jenis VALUES("7","Broadcast","7","Barang Broadcast");
INSERT INTO jenis VALUES("8","Lainnya","8","Barang Lainnya");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(100) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","Admin");
INSERT INTO level VALUES("2","Operator");
INSERT INTO level VALUES("3","Pegawai");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(100) NOT NULL,
  `nip` int(11) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("1","Rizki Muhtiani Putri","12122","Bogor");
INSERT INTO pegawai VALUES("2","Rheina Apriliana","13625221","Bogor");
INSERT INTO pegawai VALUES("3","Nabila Silfani","218282312","Jakarta");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(15) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("19","65","","2019-04-05 11:12:52","2019-04-05 11:13:06","Dikembalikan","2");
INSERT INTO peminjaman VALUES("20","65","","2019-04-06 14:46:57","0000-00-00 00:00:00","Dipinjam","1");
INSERT INTO peminjaman VALUES("21","73","","2019-04-06 16:07:22","0000-00-00 00:00:00","Dipinjam","1");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","admin","admin","Admin","Admin");
INSERT INTO petugas VALUES("2","operator","operator","Operator","Operator");
INSERT INTO petugas VALUES("3","muhtianiputri","muhtiani123","Muhtiani Putri","Pegawai");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(20) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Lab RPL","KR-001","RPL");
INSERT INTO ruang VALUES("2","Bengkel TKR","KR-002","TKR");
INSERT INTO ruang VALUES("3","Lab Animasi","KR-003","Animasi");
INSERT INTO ruang VALUES("4","Ruang Las","KR-004","TPL");
INSERT INTO ruang VALUES("5","Ruang Broadcast","KR-005","BC");
INSERT INTO ruang VALUES("6","Lainnya","KR-006","Barang Umum");
INSERT INTO ruang VALUES("7","Ruang TU","KR-007","Tata Usaha");



