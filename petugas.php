<?php
include "head.php";
include "koneksi.php";
?>

<section class="content">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading"><b><center>Petugas</center></b>
				<div class="panel-body">
					<div class="col-lg-12">

						<form role="form" action="" method="POST">
							<div class="form-group">
								<label>Username</label>
								<input type="text" class="form-control" id="username" name="username" autocomplete="off" required="">
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control" id="password" name="password" autocomplete="off" required="">
							</div>
							<div class="form-group">
								<label>Nama</label>
								<input type="text" class="form-control" id="nama_petugas" name="nama_petugas" autocomplete="off" required="">
							</div>
							<div class="form-group">
								<label>Level</label>
								<select name="level" class="form-control" required="">
									<option>---Pilih---</option>

									<?php
									$konek = mysqli_connect("localhost","root","","inventaris_muhti");
									$result = mysqli_query($konek, "SELECT * from level ORDER BY id_level");
									while ($row = mysqli_fetch_assoc($result)) 
									{
										echo "<option value='$row[id_level]' >$row[nama_level]</option>";
									}
									?>

								</select>
							</div>
							<div class="box-footer">
								<input type="submit" class="btn btn-github" name="simpan" value="simpan">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
include 'koneksi.php';
if(isset($_POST['simpan']))
{
	$username=$_POST['username'];
	$password=$_POST['password'];
	$nama_petugas=$_POST['nama_petugas'];
	$level=$_POST['level'];

	$input=mysqli_query($konek, "INSERT INTO petugas (username, password, nama_petugas, level)VALUES('$username', '$password', '$nama_petugas', '$level')");
	if ($input) {
		echo "Berhasil";
		?>
		<script type="text/javascript">
			window.location.href="petugas.php";
		</script>
		<?php
	}else{
		echo"gagal";
	}
}
?> 

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Petugas</h3>                                    
				</div>

				<div class="box-body table-responsive">

					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Username</th>
								<th>Nama</th>
								<th>Level</th>
								<th>Option</th>
							</tr>
						</thead>

						<?php
						include "koneksi.php";
						$no=1;
						$pilih=mysqli_query($konek, "SELECT * FROM petugas ORDER BY id_petugas ASC");
						while($data=mysqli_fetch_array($pilih)){
							?>
							<tbody>
								<tr>
									<td><?php echo $no; ?></td>
									<td><?=$data['username']; ?></td>
									<td><?=$data['nama_petugas'];?></td>
									<td><?=$data['level'];?></td>
									<td>
										<a class="btn btn-github" href="edit_petugas.php?id_petugas=<?php
										echo $data['id_petugas'];?>"><i class="fa fa-edit"></i></a>
										<a onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data Ini?')"
										class="btn btn-danger" href="hapus_petugas.php?id_petugas=<?php echo $data['id_petugas'];?>"><i class="fa fa-trash-o"></i></a> 
									</td>
								</tr>
							</tbody>
							<?php 
							$no++;    
						} 
						?>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include "footer.php"; ?>