<?php
include "head.php";
include "koneksi.php";
?>

<section class="content">
	<div class="row">
		<div class="box box-primary">
		<div class="box-header">
		<h3 class="box-title">Tambah Barang</h3>
		</div>
		<div class="col-lg-6">

			<form role="form" action="" method="POST">
				<div class="form-group">
					<label>Nama Barang</label>
					<input type="text" class="form-control" id="nama" name="nama" required="" autocomplete="off">
				</div>
				<div class="form-group">
					<label>Kondisi</label>
					<select name="kondisi" class="form-control" required="">
						<option>---Pilih---</option>
						<option>Baik</option>
						<option>Rusak</option>
					</select>
				</div>
				<div class="form-group">
					<label>Keterangan</label>
					<input type="text" class="form-control" id="keterangan" name="keterangan" maxlength="40" required="" autocomplete="off">
					
				</div>
				<div class="form-group">
					<label>Jumlah</label>
					<input type="number" class="form-control" id="jumlah" name="jumlah" required="" autocomplete="off">
				</div>
			</div>

			<div class="col-lg-6">
				<div class="form-group">
					<label>Jenis</label>
					<select name="id_jenis" class="form-control" required="">
						<option>---Pilih---</option>

						<?php
						$konek = mysqli_connect("localhost","root","","inventaris_muhti");
						$result = mysqli_query($konek, "SELECT * from jenis ORDER BY id_jenis");
						while ($row = mysqli_fetch_assoc($result)) 
						{
							echo "<option value='$row[id_jenis]' >$row[nama_jenis]</option>";
						}
						?>

					</select>
				</div>

				<div class="form-group">
					<label>Ruang</label>
					<select name="id_ruang" class="form-control" required="">
						<option>---Pilih---</option>

						<?php
						$konek = mysqli_connect("localhost","root","","inventaris_muhti");
						$result = mysqli_query($konek, "SELECT * from ruang ORDER BY id_ruang");
						while ($row = mysqli_fetch_assoc($result)) 
						{
							echo "<option>$row[nama_ruang]</option>";
						}
						?>

					</select>
				</div>

				<div class="form-group">
					<label>Kode Inventaris</label>
					<input type="text" class="form-control" id="kode_inventaris" name="kode_inventaris" maxlength="8" required="" autocomplete="off">
				</div>
				<div class="form-group">
					<label>Petugas</label>
					<select name="id_petugas" class="form-control" required="">
						<option readonly="">Admin</option>

					</select>
				</div>
			</div>
			<div class="box-footer">
				<input type="submit" class="btn btn-github" name="simpan" value="simpan">
			</div>
		</form>
	</div>
</div>
</div>
</div>
</div>
</section>
<?php include "footer.php"; ?>

<?php
include 'koneksi.php';
if(isset($_POST['simpan']))
{
	$nama=$_POST['nama'];
	$kondisi=$_POST['kondisi'];
	$keterangan=$_POST['keterangan'];
	$jumlah=$_POST['jumlah'];
	$id_jenis=$_POST['id_jenis'];
	$tanggal_register=date("Y-m-d H:i:s");
	$id_ruang=$_POST['id_ruang'];
	$kode_inventaris=$_POST['kode_inventaris'];
	$id_petugas=$_POST['id_petugas'];

	$input=mysqli_query($konek, "INSERT INTO inventaris (nama, kondisi, keterangan, jumlah, id_jenis, tanggal_register, id_ruang, kode_inventaris, id_petugas)VALUES('$nama', '$kondisi', '$keterangan','$jumlah', '$id_jenis','$tanggal_register','$id_ruang','$kode_inventaris','$id_petugas')");
	if ($input) {
		echo "Berhasil";
		?>
		<script type="text/javascript">
			window.location.href="admin.php";
		</script>
		<?php
	}else{
		echo"gagal";
	}
}
?> 