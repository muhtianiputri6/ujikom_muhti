<?php
include "head.php";
include "koneksi.php";
?>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Pengembalian</h3>                                    
				</div>

					<a class="btn btn-github" style="margin-left: 10px;" href="export2.php" target="_BLANK">
                Cetak Excel
                </a>
                <a class="btn btn-primary" style="margin-left: 10px;" href="pdf_kembali.php" target="_BLANK">
                Cetak PDF
                </a>

					<div class="box-body table-responsive">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama Barang</th>
								<th>Tanggal Kembali</th>
								<th>Status</th>
								<th>Peminjam</th>
							</tr>
						</thead>

						<?php
						include "koneksi.php";
						$no=1;
						$pilih=mysqli_query($konek, "SELECT * FROM peminjaman left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai JOIN detail_pinjam ON detail_pinjam.id_peminjaman=peminjaman.id_peminjaman LEFT JOIN inventaris on peminjaman.id_inventaris=inventaris.id_inventaris where status_peminjaman='Dikembalikan' order by tanggal_kembali desc");	
						while($data=mysqli_fetch_array($pilih)){
							?>
							<tbody>
								<tr>
									<td><?=$no++;?></td>
									<td><?=$data['nama'];?></td>
									<td><?=$data['tanggal_kembali'];?></td>
									<td><?=$data['status_peminjaman'];?></td>
									<td><?=$data['nama_pegawai'];?></td>

								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include "footer.php"; ?>