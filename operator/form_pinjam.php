<?php
include "head.php";
include "koneksi.php"; 
?>

<section class="content">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading"><b><center>Peminjaman</center><b>
                <div class="panel-body">
                    <div class="col-lg-12">

                        <form role="form" action="proses_pinjam.php" method="POST">
                            <div class="form-group">
                                <label>Nama Barang</label>
                                <select name="id_inventaris" class="form-control" required="">
                                    <option>---Pilih---</option>
                                    <?php 
                                    include "koneksi.php";
                                    $select = mysqli_query($konek,"SELECT * FROM inventaris WHERE id_jenis = '$_GET[id_jenis]'");
                                    while ($data = mysqli_fetch_array($select)) 
                                    {
                                        ?>
                                        <center>
                                            <option value="<?php echo $data['id_inventaris'];?>"><?php echo $data['nama'];?></option>
                                        </center>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Jumlah</label>
                                    <input type="number" class="form-control" placeholder="enter jumlah" id="jumlah" name="jumlah" required="">
                            </div>
                            <div class="form-group">
                                <label>Nama Pegawai</label>
                                    <select class="form-control" name="id_pegawai" required="">
                                        <option>-- Pilih --</option>
                                    <?php
                                    $konek = mysqli_connect("localhost","root","","inventaris_muhti");
                                    $result = mysqli_query($konek, "select id_pegawai, nama_pegawai from pegawai ORDER BY id_pegawai");
                                    while ($row = mysqli_fetch_assoc($result)) 
                                    {
                                        echo "<option value='$row[id_pegawai]'>$row[nama_pegawai]</option>";
                                    }
                                    ?>

                                 </select>
                            </div>

                            <div class="box-footer">
                                <input type="submit" class="btn btn-primary" name="simpan" value="simpan">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
