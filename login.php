<?php
session_start();
include "koneksi.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>
	<div class="loginbox">
		<img src="img/admin.png" class="avatar">
		<h1>Login</h1>
		<form action="" method="POST">
			<p>Username</p>
			<input type="text" name="username" autocomplete="off">
			<p>Password</p>		
			<input type="password" name="password" autocomplete="off">
			<input type="submit" name="login" value="Login">
			<a href="lupapass.php">Lupa Password?</a>
			<br>
			<a href="help/help.php">Help</a>
		</form>
		<?php
			if (isset($_POST['login'])) {
				$username = $_POST['username'];
				$password = $_POST['password'];
				$login = mysqli_query($konek, "SELECT * from petugas where username='$username' and password='$password'");
				$cek = mysqli_num_rows($login);
				if ($cek > 0){
					$data = mysqli_fetch_assoc($login);
					if($data['level']=="Admin"){
						$_SESSION['username'] = $username;
						$_SESSION['level'] = "Admin";
						header("location:data_barang.php");
					}else if ($data['level']=="Operator"){
						$_SESSION['username'] = $username;
						$_SESSION['level'] = "Operator";
						header("location:operator/peminjaman.php");

					}else if ($data['level']=="Pegawai"){
						$_SESSION['username'] = $username;
						$_SESSION['level'] = "Pegawai";
						header("location:user/peminjaman.php");
					}else{
						header("location:login.php");
					}
				}else{
					//tetap pada halaman login
					header("location:login.php");
					}		
			}
		?>
	</div>
</body>
</html>