<?php
include 'koneksi.php';
include 'pdf/fpdf.php';

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);

$pdf->SetX(13);
$pdf->MultiCell(19.05,0.5,'SMKN 1 CIOMAS',0,'L');
$pdf->SetX(12.05);
$pdf->MultiCell(19.05,0.5,'Telpon : 0812XXXXXXXX',0,'L');
$pdf->SetFont('Arial','B',10);
$pdf->SetX(10);
$pdf->MultiCell(19.05,0.5,'Jl. Laladon Desa Laladon RT 02/01 Kec. Ciomas Bogor',0,'L');
$pdf->SetX(12);
$pdf->MultiCell(19.05,0.5,'Website : wwww.smkn1ciomas.com',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);
$pdf->Line(1,3.2,28.5,3.2);
$pdf->SetLineWidth(0);
$pdf->Line(1,3.1,28.5,3.1);
$pdf->ln(1);

$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Barang",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama Barang', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'Kondisi', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Keterangan', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'Jumlah', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama Jenis', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Tgl Regis', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama Ruang', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Kode Inven', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama Petugas', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$query=mysqli_query($konek, "SELECT * FROM inventaris");
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['nama'],1, 0, 'C');
	$pdf->Cell(2, 0.8, $lihat['kondisi'], 1, 0,'C');
	$pdf->Cell(3, 0.8, $lihat['keterangan'], 1, 0,'C');
	$pdf->Cell(2, 0.8, $lihat['jumlah'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['id_jenis'],1, 0, 'C');
	$pdf->Cell(4, 0.8, $lihat['tanggal_register'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['id_ruang'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['kode_inventaris'],1, 0, 'C');
	$pdf->Cell(3., 0.8, $lihat['id_petugas'],1, 1, 'C');

	$no++;
}

$pdf->Output("laporan_inventaris.pdf","I");

?>


