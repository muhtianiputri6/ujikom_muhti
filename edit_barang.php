<?php
include "head.php";
include "koneksi.php";
?>

<?php
include"koneksi.php";
$id_inventaris=$_GET['id_inventaris'];
$pilih=mysqli_query($konek, "SELECT * FROM inventaris WHERE id_inventaris='$id_inventaris'");
$tampil=mysqli_fetch_array($pilih);
?>


<section class="content">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading"><b><center>Edit Barang</center></b>
				<div class="panel-body">
					<div class="col-lg-6">

						<form role="form" action="" method="POST">
							<div class="form-group">
								<label>Nama Barang</label>
								<input type="hidden" name="id_inventaris" value="<?php echo $_GET['id_inventaris'];?>">
								<input type="text" class="form-control" id="nama" name="nama" value="<?php echo $tampil['nama'];?>">
							</div>
							<div class="form-group">
								<label>Kondisi</label>
								<select name="kondisi" class="form-control" id="kondisi" value="<?php echo $tampil['kondisi'];?>">
									<option>Baik</option>
									<option>Rusak</option>
								</select>
							</div>
							<div class="form-group">
								<label>Keterangan</label>
								<input type="text" class="form-control" id="keterangan" name="keterangan" value="<?php echo $tampil['keterangan'];?>">
							</div>
							<div class="form-group">
								<label>Jumlah</label>
								<input type="text" class="form-control" id="jumlah" name="jumlah" value="<?php echo $tampil['jumlah'];?>">
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label>Jenis</label>
								<input type="text" class="form-control" id="id_jenis" name="id_jenis" value="<?php echo $tampil['id_jenis'];?>">
							</div>

							<div class="form-group">
								<label>Ruang</label>
								<input type="text" class="form-control" id="id_ruang" name="id_ruang" value="<?php echo $tampil['id_ruang'];?>">
							</div>

						<div class="form-group">
							<label>Kode Inventaris</label>
							<input type="text" class="form-control" id="kode_inventaris" name="kode_inventaris" value="<?php echo $tampil['kode_inventaris'];?>">
						</div>
						
							<div class="form-group">
								<label>Petugas</label>
								<input type="text" class="form-control" id="id_petugas" name="id_petugas" value="<?php echo $tampil['id_petugas'];?>">
							</div>
					</div>
					<div class="box-footer">
						<input type="submit" class="btn btn-primary" name="edit" value="edit">
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
</div>
</section>
<?php include "footer.php"; ?>

<?php
include"koneksi.php";
if(isset($_POST['edit'])){
	$id_inventaris=$_POST['id_inventaris'];
	$nama=$_POST['nama'];
	$kondisi=$_POST['kondisi'];
	$keterangan=$_POST['keterangan'];
	$jumlah=$_POST['jumlah'];
	$id_jenis=$_POST['id_jenis'];
	$id_ruang=$_POST['id_ruang'];
	$kode_inventaris=$_POST['kode_inventaris'];
	$id_petugas=$_POST['id_petugas'];

	$input=mysqli_query($konek, "UPDATE inventaris SET nama='$nama', kondisi='$kondisi', keterangan='$keterangan', jumlah='$jumlah', id_jenis='$id_jenis', id_ruang='$id_ruang', kode_inventaris='$kode_inventaris', id_petugas='$id_petugas' WHERE id_inventaris='$id_inventaris'");

	if ($input) {
		echo "Berhasil";
		?>
		<script type="text/javascript">
			window.location.href="admin.php";
		</script>
		<?php
	}else{
		echo"gagal";
	}
}
?>